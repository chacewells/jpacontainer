package containerspringjpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import containerspringjpa.model.Container;
import containerspringjpa.service.ContainerService;

@RestController
@RequestMapping("/jsoncontainer/")
public class ContainerRestController {
	
	@Autowired
	ContainerService containerService;
	
	@RequestMapping(value="/item")
	public Container showGetContainer(@RequestParam("id") int id) {
		return containerService.getContainer(id);
	}
	
}
