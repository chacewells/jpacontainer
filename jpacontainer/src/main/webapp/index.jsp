<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:url value="/jsoncontainer/item" var="fake"/>

<html>
	<head>
		<meta charset="utf-8">
		<title>Welcome</title>
		<style>
			table, th, td {
				border: thin solid gray;
			}
			table#fakeBookingTable {
				display:none;
			}
		</style>
	</head> 
	<body>
		Enter a container id:<br/>
		<input type="text" id="fetch_id"/>
		<button type="button" id="fetch_button">Show</button>
		<table id="fakeBookingTable"></table>
	<script src='<c:url value="/static/js/jquery-2.1.3.js"/>'></script>
	<script>
		var populateTable = function (fetch_id) {
			$('table#fakeBookingTable').html('').hide();
			$.getJSON('${fake}?id=' + fetch_id, function (data) {
				(function () {
					var counter = 0;
					console.log(data.id);
					$.each(data, function(key, value) {
						var tr = $('<tr>');
							tr.append('<td>' + key + '</td>');
							tr.append('<td>' + value + '</td>');
						$('table#fakeBookingTable')
						.append(tr)
						.show();
					});
				})();
			});
		};
		
		$(document).ready(function () {
			$('#fetch_button').click(function () {
				populateTable( $('#fetch_id').val() );
			});
		});
	</script>
	</body>
</html>
