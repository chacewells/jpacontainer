package containerspringjpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CONTAINER")
public class Container {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="DISPLAY_NAME")
	private String displayName;
	
	@Column(name="REMAINING_VOLUME")
	private double remainingVolume;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public double getRemainingVolume() {
		return remainingVolume;
	}
	public void setRemainingVolume(double remainingVolume) {
		this.remainingVolume = remainingVolume;
	}
}
