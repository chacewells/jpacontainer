package containerspringjpa.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;

import containerspringjpa.model.Container;

@Service
public class ContainerService {
	@PersistenceContext
	EntityManager entityManager;
	
	public Container getContainer(int id) {
		return entityManager.find(Container.class, id);
	}
}
