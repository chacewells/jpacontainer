package containerspringjpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import containerspringjpa.model.Container;
import containerspringjpa.service.ContainerService;

@Controller
@RequestMapping("/container/")
public class ContainerController {
	
	@Autowired
	ContainerService service;
	
//	@RequestMapping(method=RequestMethod.GET, value="/add.html")
//	public ModelAndView showAddContainer() {
//		ModelAndView mav = new ModelAndView("addContainer");
//		
//		mav.addObject("container", new Container());
//		
//		return mav;
//	}
//	
//	@RequestMapping(method=RequestMethod.POST, value="/add.html")
//	public ModelAndView addContainer(@ModelAttribute("container") Container container,
//			BindingResult result,
//			Model m) {
//		ModelAndView mav = new ModelAndView("showContainer");
//		service.addContainer(container);
//		
//		return mav;
//	}
	
	@RequestMapping(method=RequestMethod.GET, value="/show.html")
	public ModelAndView showContainer(@RequestParam(value="container_id", defaultValue="3") int id) {
		ModelAndView mav = new ModelAndView("showContainer");
		Container container = service.getContainer(id);
		mav.addObject("container", container);
		return mav;
	}
	
}
